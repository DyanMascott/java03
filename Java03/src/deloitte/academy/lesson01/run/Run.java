package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.academy.lesson01.category.Category;
import deloitte.academy.lesson01.product.Product;

/**
 * Main class of the project Java03 Creation of an Online Shop
 * 
 * @author dmascott
 *
 */
public class Run {

	/**
	 * Main class of the project Java03
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * The object Logger is initialized, it will obtain the method name as a value
		 */
		final Logger LOGGER = Logger.getLogger(Run.class.getName());

		/*
		 * An ArrayList with Product values is created
		 */
		ArrayList<Product> productList = new ArrayList<Product>();

		/*
		 * The objects are instantiated
		 */
		Product product = new Product();
		Product product1 = new Product();
		Product product2 = new Product();
		Product product3 = new Product();
		Product product4 = new Product();

		/*
		 * The products are created
		 */
		product.setProdName("Lenovo Computer T430");
		product.setProdCode(1003);
		product.setCatName(Category.Electronics.name());
		product.setCatCode(Category.Electronics.code);
		product.setProdSold(21);
		product.setProdStock(12);

		product1.setProdName("Camera Nikon D5600");
		product1.setProdCode(1005);
		product1.setCatName(Category.Electronics.name());
		product1.setCatCode(Category.Electronics.code);
		product1.setProdSold(15);
		product1.setProdStock(14);

		product2.setProdName("Jenga");
		product2.setProdCode(2001);
		product2.setCatName(Category.Toys.name());
		product2.setCatCode(Category.Toys.code);
		product2.setProdSold(30);
		product2.setProdStock(15);

		product3.setProdName("Xbox One Inalambric Control");
		product3.setProdCode(2054);
		product3.setCatName(Category.Videogames.name());
		product3.setCatCode(Category.Videogames.code);
		product3.setProdSold(27);
		product3.setProdStock(23);

		product4.setProdName("Classic Agenda 2020 Blue");
		product4.setProdCode(3884);
		product4.setCatName(Category.Office.name());
		product4.setCatCode(Category.Office.code);
		product4.setProdSold(13);
		product4.setProdStock(17);

		/*
		 * The products are added to the ArrayList "productList"
		 */
		productList.add(product);
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		productList.add(product4);

		System.out.println("Total Products");
		/*
		 * A String variable to store the value "on stock" or "sold" is created
		 */
		String inventary = "on stock";

		/*
		 * The method totalProduct of the class Product is called
		 */

		int totalProduct = Product.totalProduct(productList, inventary);
		LOGGER.info("The total of product " + inventary + " is: " + totalProduct + "\n");

		/*
		 * The method arrayTotalProducts of the class Product is called
		 */
		ArrayList<String> totalProduct1 = Product.arrayTotalProducts(productList, inventary);
		LOGGER.info("The total of products " + inventary + " is: " + totalProduct1 + "\n");

		/*
		 * The method productSold of the class Product is called
		 */
		String soldProds = Product.productSold(productList, 2, product1.getProdCode());
		LOGGER.info(soldProds + "\n");

		System.out.println("Search by category");
		/*
		 * A String variable to store the name of the category is created
		 */
		String catName = Category.Toys.name();

		/*
		 * An int variable to store the code of the category is created
		 */
		int catCode = Category.Electronics.getCode();

		/*
		 * The method searchByCatName of the class product is called
		 */
		ArrayList<String> productByName = Product.searchByCatName(productList, catName);
		LOGGER.info("The products in the category " + catName + ": " + productByName + "\n");

		/*
		 * The method searchByCatCode of the class product is called
		 */
		ArrayList<String> productByCode = Product.searchByCatCode(productList, catCode);
		LOGGER.info("The products in the category " + catCode + ": " + productByCode + "\n");

		System.out.println("New Product Saved");

		/*
		 * Set values to the new product
		 */
		Product product5 = new Product();

		product5.setProdName("Red Notebook");
		product5.setProdCode(3005);
		product5.setCatName(Category.Office.name());
		product5.setCatCode(Category.Office.code);
		product5.setProdSold(12);
		product5.setProdStock(5);

		/*
		 * Create a new Product
		 */
		productList.add(product5);
		String saved = product.newProd(product5);
		LOGGER.info(saved);
	}

}
