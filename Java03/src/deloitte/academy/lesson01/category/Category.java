package deloitte.academy.lesson01.category;

/**
 * Enum Category
 * 
 * @author dmascott
 *
 */
public enum Category {

	/*
	 * The Definition of the different categories
	 */
	Electronics(1000), Toys(2000), Videogames(3000), Office(4000);

	/*
	 * Constructor of the enum
	 */
	private Category(int code) {
		this.code = code;
	}

	/*
	 * A property code for the category
	 */
	public int code;

	/*
	 * Getter and setter of the property code from category
	 */
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
