package deloitte.academy.lesson01.product;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Product Exercise
 * 
 * @author dmascott
 *
 */
public class Product {

	/**
	 * The constructor of the object Product is created
	 * 
	 * @param prodName  String value
	 * @param prodStock int value
	 * @param prodSold  int value
	 * @param prodCode  int value
	 * @param catName   String value
	 * @param catCode   int value
	 */
	public Product(String prodName, int prodStock, int prodSold, int prodCode, String catName, int catCode) {
		super();
		this.prodName = prodName;
		this.prodStock = prodStock;
		this.prodSold = prodSold;
		this.prodCode = prodCode;
		this.catName = catName;
		this.catCode = catCode;
	}

	/**
	 * Superclass Constructor
	 */
	public Product() {
		super();
	}

	/*
	 * The variables are created
	 */
	public String prodName;
	public int prodStock;
	public int prodSold;
	public int prodCode;
	public String catName;
	public int catCode;

	/*
	 * Getters and Setters are created
	 */
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public int getProdStock() {
		return prodStock;
	}

	public void setProdStock(int prodStock) {
		this.prodStock = prodStock;
	}

	public int getProdSold() {
		return prodSold;
	}

	public void setProdSold(int prodSold) {
		this.prodSold = prodSold;
	}

	public int getProdCode() {
		return prodCode;
	}

	public void setProdCode(int prodCode) {
		this.prodCode = prodCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public int getCatCode() {
		return catCode;
	}

	public void setCatCode(int catCode) {
		this.catCode = catCode;
	}

	/*
	 * The object Logger is initialized, it will obtain the method name as a value
	 */
	private static final Logger LOGGER = Logger.getLogger(Product.class.getName());

	/**
	 * Method totalProduct
	 * 
	 * @param productList An array with Product values
	 * @param quantity    String value with the word "stock" or "sold"
	 * @return An int value with the quantity of products
	 * 
	 *         Returns the total of products on stock or sold, depending on the
	 *         value send in the parameter quantity
	 */
	public static int totalProduct(ArrayList<Product> productList, String quantity) {
		int total = 0;
		try {
			if (quantity == "sold") {
				for (Product product : productList) {
					total = total + product.getProdSold();
				}
			} else if (quantity == "on stock") {
				for (Product product : productList) {
					total = total + product.getProdStock();
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return total;
	}

	/**
	 * Method productSold
	 * 
	 * @param productList An array with Product values
	 * @param quantity    int value with the quantity sold
	 * @param productCode int value with the code of the product sold
	 * @return A String value with the product updated
	 * 
	 *         Returns a String with the name of the product, the initial quantity
	 *         on stock of the product, the initial quantity of that product sales,
	 *         the updated quantity on stock and the updated quantity of product
	 *         sales
	 */
	public static String productSold(ArrayList<Product> productList, int quantity, int productCode) {
		String sold = "";
		try {
			for (Product product : productList) {
				if (product.getProdCode() == productCode) {
					int stockUpdt = product.getProdStock() - quantity;
					int soldUpdt = product.getProdSold() + quantity;
					sold = "Product: " + product.getProdName() + "\n" + "Initial product on stock: "
							+ product.getProdStock() + "\n" + "Initial product sold: " + product.getProdSold() + "\n"
							+ "Stock Updated: " + stockUpdt + "\n" + "Sold products updated: " + soldUpdt;
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return sold;
	}

	/**
	 * Method arrayTotalProducts
	 * 
	 * @param productList An array with Product values
	 * @param quantity    String value with the word "stock" or "sold"
	 * @return An array with String values with the Name of the product and quantity
	 * 
	 *         Returns the name of the product and total of products on stock or
	 *         sold, depending on the value send in the parameter quantity
	 */
	public static ArrayList<String> arrayTotalProducts(ArrayList<Product> productList, String quantity) {
		ArrayList<String> value = new ArrayList<String>();
		String addVal = "";
		try {
			if (quantity == "sold") {
				for (Product product : productList) {
					addVal = ("\n" + product.getProdName() + ": " + product.getProdSold());
					value.add(addVal);
				}
			} else if (quantity == "on stock") {
				for (Product product : productList) {
					addVal = ("\n" + product.getProdName() + ": " + product.getProdStock());
					value.add(addVal);
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return value;
	}

	/**
	 * Method searchByCatName
	 * 
	 * @param productList An array with Product values
	 * @param category    A String value with the name of the category
	 * @return An Array with the products that belongs to the parameter category
	 * 
	 *         Returns an array of products that belogns to the category defined in
	 *         the parameter category
	 */
	public static ArrayList<String> searchByCatName(ArrayList<Product> productList, String category) {
		ArrayList<String> values = new ArrayList<String>();

		try {
			for (Product product : productList) {
				if (product.getCatName() == category) {
					values.add(product.getProdName());
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return values;
	}

	/**
	 * Method searchByCatCode
	 * 
	 * @param productList An array with Product values
	 * @param category    An int value with the code of the category
	 * @return An Array with the products that belongs to the parameter category
	 * 
	 *         Returns an array of products that belogns to the category defined in
	 *         the parameter category
	 */
	public static ArrayList<String> searchByCatCode(ArrayList<Product> productList, int category) {
		ArrayList<String> values = new ArrayList<String>();

		try {
			for (Product product : productList) {
				if (product.getCatCode() == category) {
					values.add(product.getProdName());
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}

		return values;
	}

	/**
	 * Method newProd
	 * 
	 * @param newProduct Getters of the object product
	 * @return Object Setters of the object product
	 * 
	 *         Creates a new product
	 */
	public String newProd(Product newProduct) {
		Product product = new Product();
		String value = "";

		product.setProdName(newProduct.getProdName());
		product.setProdCode(newProduct.getProdCode());
		product.setProdSold(newProduct.getProdSold());
		product.setProdStock(newProduct.getProdStock());
		product.setCatName(newProduct.getCatName());
		product.setCatCode(newProduct.getCatCode());

		value = "New Product Registered: " + product.getProdName();

		return value;
	}

}
